/*
 * Copyright (c) 2006-2012 Washington University
 */
package org.nrg.dcm.db;

import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;
import org.dcm4che2.data.Tag;
import org.junit.Test;
import org.nrg.attr.ConversionFailureException;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.*;

public class FileSetTest {
    // Uses sample1 dataset
    final static private File   DATA_DIRECTORY            = new File("src/test/resources/dicom");
    final static private int    numImageFiles             = 27;
    final static private int    numSequences              = 2;
    final static private int    numSeries                 = 3;
    final static private int    numReferringPhys          = 1;
    final static private String aSequenceName             = "*tfl3d1_ns";
    final static private int    numInASequence            = 18;
    final static private int    numSeriesInASequence      = 2;
    final static private String oneSeriesInASequence      = "4";
    final static private int    numInOneSeriesInASequence = 9;
    final static private String modelName                 = "TrioTim";
    final static private String aSeriesNumber             = "5";
    final static private int    numInASequenceSeries      = 9;

    private final static File    DATA_FILE_1;
    private final static File    DATA_FILE_2;
    private final static File    DATA_FILE_3;
    private static       FileSet DATA_FILE_SET;

    static {
        try {
            DATA_FILE_SET = new FileSet(DATA_DIRECTORY);
        } catch (IOException | SQLException e) {
            fail("unable to load FileSet " + DATA_DIRECTORY + " : " + e.getMessage());
            throw new RuntimeException("fail failed?");
        }

        try {
            DATA_FILE_1 = new File(DATA_DIRECTORY, "1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm.gz").getCanonicalFile();
            DATA_FILE_2 = new File(DATA_DIRECTORY, "1.MR.head_DHead.4.2.20061214.091206.156000.0918517980.dcm.gz").getCanonicalFile();
            DATA_FILE_3 = new File(DATA_DIRECTORY, "1.MR.head_DHead.4.3.20061214.091206.156000.0506717986.dcm.gz").getCanonicalFile();
        } catch (IOException e) {
            fail("unable to get canonical test files: " + e.getMessage());
            throw new RuntimeException("fail failed?");
        }
    }

    @Test
    public final void testEquals() {
        assertEquals(DATA_FILE_SET, DATA_FILE_SET);
        assertNotEquals(DATA_FILE_SET, null);
        final FileSet dataDirectories;
        try {
            dataDirectories = new FileSet(DATA_DIRECTORY);
        } catch (IOException | SQLException e) {
            fail(e.getMessage());
            return;
        }
        assertEquals(DATA_FILE_SET, dataDirectories);
        assertEquals(dataDirectories, DATA_FILE_SET);
        dataDirectories.dispose();
    }

    @Test
    public final void testFileSet() throws Exception {
        final FileSet dataDirectories = new FileSet(DATA_DIRECTORY);
        assertNotNull(dataDirectories);
        dataDirectories.dispose();
    }

    static public class Patient extends DirectoryRecord {
        private static Set<Patient> all   = new HashSet<>();
        private static int          count = 0;

        Patient(final DirectoryRecord upper, final Map<Integer, String> vals) {
            super(DirectoryRecord.Template.PATIENT, upper, vals);
            if (all.add(this)) {
                count++;
            }
        }

        static int getCount() {
            return count;
        }
    }

    static public class Study extends DirectoryRecord {
        private static Set<Study> all   = new HashSet<>();
        private static int        count = 0;

        Study(final DirectoryRecord upper, final Map<Integer, String> vals) {
            super(DirectoryRecord.Template.STUDY, upper, vals);
            if (all.add(this)) {
                count++;
            }
        }

        static int getCount() {
            return count;
        }
    }

    static public class Series extends DirectoryRecord {
        private static Set<Series> all   = new HashSet<>();
        private static int         count = 0;

        Series(final DirectoryRecord upper, final Map<Integer, String> vals) {
            super(DirectoryRecord.Template.SERIES, upper, vals);
            if (all.add(this)) {
                count++;
            }
        }

        static int getCount() {
            return count;
        }
    }

    static public class Instance extends DirectoryRecord {
        private static Set<Instance> all   = new HashSet<>();
        private static int           count = 0;

        Instance(final DirectoryRecord upper, final Map<Integer, String> vals) {
            super(DirectoryRecord.Template.INSTANCE, upper, vals);
            if (all.add(this)) {
                count++;
            }
        }

        static int getCount() {
            return count;
        }

        File getFile() {
            return new File(getValue(Tag.ReferencedFileID));
        }

        @Override
        public String toString() {
            return super.toString() + " : " + getFile().getPath();
        }
    }


    @Test
    public final void testFileSetWithRecordConstructors() throws Exception {
        FileSet dds1 = new FileSet(DATA_DIRECTORY, null);
        assertNotNull(dds1);
        assertNull(dds1.getPatientDirRecords());
        dds1.dispose();

        DirectoryRecord.Factory factory = new DirectoryRecord.Factory() {
            public DirectoryRecord newInstance(DirectoryRecord.Type type, DirectoryRecord upper, final Map<Integer, String> vals) {
                if (type == DirectoryRecord.Type.INSTANCE) {
                    return new Instance(upper, vals);
                }
                if (type == DirectoryRecord.Type.SERIES) {
                    return new Series(upper, vals);
                }
                if (type == DirectoryRecord.Type.STUDY) {
                    return new Study(upper, vals);
                }
                if (type == DirectoryRecord.Type.PATIENT) {
                    return new Patient(upper, vals);
                }
                throw new IllegalArgumentException("invalid DirectoryRecord type");
            }

            public Collection<Integer> getSelectionKeys(DirectoryRecord.Type type) {
                return DirectoryRecord.getDefaultFactory().getSelectionKeys(type);
            }
        };

        final FileSet dataDirectories = new FileSet(DATA_DIRECTORY, new HashMap<>(), factory);
        assertNotNull(dataDirectories);

        assertEquals(1, Patient.getCount());
        assertEquals(1, Study.getCount());
        assertEquals(numSeries, Series.getCount());
        assertTrue(numImageFiles <= Instance.getCount()); // may be duplicates

        final Set<DirectoryRecord> patients = dataDirectories.getPatientDirRecords();
        assertNotNull(patients);
        assertEquals(1, patients.size());
        final DirectoryRecord patient = patients.iterator().next();
        assertNotNull(patient);
        assertEquals(DirectoryRecord.Type.PATIENT, patient.getType());
        assertNull(patient.getUpper());
        assertEquals(1, patient.getLower().size());

        DirectoryRecord study = patient.getLower().iterator().next();
        assertEquals(DirectoryRecord.Type.STUDY, study.getType());
        assertTrue(study instanceof Study);
        assertEquals(patient, study.getUpper());

        DirectoryRecord series = study.getLower().iterator().next();
        assertEquals(DirectoryRecord.Type.SERIES, series.getType());
        assertTrue(series instanceof Series);
        assertEquals(study, series.getUpper());

        final DirectoryRecord directoryRecord = series.getLower().iterator().next();
        assertTrue(directoryRecord instanceof Instance);
        final Instance instance = (Instance) directoryRecord;
        assertEquals(DirectoryRecord.Type.INSTANCE, instance.getType());
        assertEquals(0, instance.getLower().size());
        assertNotNull(instance.getFile());
        assertTrue(dataDirectories.getDataFiles().contains(instance.getFile()));

        dataDirectories.dispose();
    }


    @Test
    public final void testGetDataFiles() throws Exception {
        Set<File> files = DATA_FILE_SET.getDataFiles();
        assertTrue(numImageFiles <= files.size());  // may be duplicates
    }

    @Test
    public final void testGetFilesForValues() throws Exception {
        if (DATA_FILE_SET != null) {
            DATA_FILE_SET.dispose();
        }
        DATA_FILE_SET = new FileSet(DATA_DIRECTORY);    // make sure we have clean cache
        assertTrue(numImageFiles <= DATA_FILE_SET.size());  // may be duplicates

        final Map<Integer, String> attrValues = new HashMap<>();

        final Map<Integer, ConversionFailureException> failures = new HashMap<>();
        Set<File>                                      files    = DATA_FILE_SET.getFilesForValues(attrValues, failures);
        assertTrue(failures.isEmpty());
        assertTrue(numImageFiles <= files.size());    // may be duplicates

        attrValues.put(Tag.SequenceName, aSequenceName);
        files = DATA_FILE_SET.getFilesForValues(attrValues, failures);
        assertTrue(failures.isEmpty());
        assertTrue(numInASequence <= files.size());   // may be duplicates

        attrValues.put(Tag.ManufacturerModelName, modelName);
        files = DATA_FILE_SET.getFilesForValues(attrValues, failures);
        assertTrue(failures.isEmpty());
        assertTrue(numInASequence <= files.size());   // may be duplicates

        attrValues.put(Tag.SeriesNumber, aSeriesNumber);
        files = DATA_FILE_SET.getFilesForValues(attrValues, failures);
        assertTrue(failures.isEmpty());
        assertTrue(numInASequenceSeries <= files.size()); // may be duplicates

        // Now make sure that it works with cached values
        DATA_FILE_SET.getUniqueValues(Tag.SequenceName);
        DATA_FILE_SET.getUniqueValues(Tag.SeriesNumber);
        files = DATA_FILE_SET.getFilesForValues(attrValues, failures);
        assertTrue(failures.isEmpty());
        assertTrue(numInASequenceSeries <= files.size());
    }

    @Test
    public final void testGetUniqueCombinations() throws Exception {
        final Map<Integer, ConversionFailureException> failures = new HashMap<>();
        final Set<Integer>                             tags     = new HashSet<>();

        tags.add(Tag.SequenceName);
        final Set<Map<Integer, String>> combs = DATA_FILE_SET.getUniqueCombinations(tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(numSequences, combs.size());

        tags.add(Tag.SeriesNumber);
        combs.clear();
        combs.addAll(DATA_FILE_SET.getUniqueCombinations(tags, failures));
        assertTrue(failures.isEmpty());
        assertEquals(numSeries, combs.size());

        tags.add(Tag.OperatorsName);
        try {
            combs.clear();
            combs.addAll(DATA_FILE_SET.getUniqueCombinations(tags, failures));
            assertTrue(failures.isEmpty());
        } catch (IOException e) {
            fail(e.getMessage());
        }
        assertEquals(numSeries, combs.size());
    }

    @Test
    public final void testGetUniqueCombinationsGivenValues() throws Exception {
        final Map<Integer, ConversionFailureException> failures = new HashMap<>();
        final Map<Integer, String>                     given    = new HashMap<>();
        final Set<Integer>                             tags     = new HashSet<>();

        // empty given, nonempty tags
        tags.add(Tag.SequenceName);
        final Set<Map<Integer, String>> combs = DATA_FILE_SET.getUniqueCombinationsGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(numSequences, combs.size());

        // Adding up the number of files per value over all values should
        // give us the total file count.
        int filesCount = 0;
        for (Map<Integer, String> comb : combs) {
            assertEquals(1, comb.size());    // we only asked for one value
            filesCount += DATA_FILE_SET.getFilesForValues(comb, failures).size();
            assertTrue(failures.isEmpty());
        }
        assertTrue(numImageFiles <= filesCount);    // may be duplicates

        // one given, one tag
        given.clear();
        given.put(Tag.SequenceName, aSequenceName);
        tags.clear();
        tags.add(Tag.SeriesNumber);

        combs.clear();
        combs.addAll(DATA_FILE_SET.getUniqueCombinationsGivenValues(given, tags, failures));
        assertTrue(failures.isEmpty());
        assertEquals(numSeriesInASequence, combs.size());

        // multiple given, one tag
        given.put(Tag.SeriesNumber, oneSeriesInASequence);
        tags.clear();
        tags.add(Tag.InstanceNumber);

        // this should give us one combination, (sequence series instance),
        // for each file in the sequence.
        combs.clear();
        combs.addAll(DATA_FILE_SET.getUniqueCombinationsGivenValues(given, tags, failures));
        assertTrue(failures.isEmpty());
        assertEquals(numInOneSeriesInASequence, combs.size());

        // multiple given, multiple tags
        tags.add(Tag.AcquisitionTime);

        // this should give us one combination, (sequence series instance time),
        // for each file in the sequence.
        combs.clear();
        combs.addAll(DATA_FILE_SET.getUniqueCombinationsGivenValues(given, tags, failures));
        assertTrue(failures.isEmpty());
        assertEquals(numInOneSeriesInASequence, combs.size());
    }

    @Test
    public final void testGetUniqueValuesFromFiles() throws Exception {
        final Map<Integer, ConversionFailureException> failures = new HashMap<>();
        final Set<File>                                files    = new HashSet<>();
        final Set<Integer>                             tags     = new HashSet<>();
        SetMultimap<Integer, String>                   result;

        result = DATA_FILE_SET.getUniqueValuesFromFiles(files, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(0, result.size());

        files.add(DATA_FILE_1);
        result = DATA_FILE_SET.getUniqueValuesFromFiles(files, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(0, result.size());

        tags.add(Tag.InstanceNumber);
        tags.add(Tag.SOPInstanceUID);
        result = DATA_FILE_SET.getUniqueValuesFromFiles(files, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(2, result.size());
        for (int tag : result.keySet()) {
            assertEquals(1, result.get(tag).size());
        }

        files.add(DATA_FILE_2);
        files.add(DATA_FILE_3);
        result = DATA_FILE_SET.getUniqueValuesFromFiles(files, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(2, result.keySet().size());
        for (int tag : result.keySet()) {
            assertEquals(3, result.get(tag).size());
        }

        tags.clear();
        tags.add(Tag.SeriesInstanceUID);
        tags.add(Tag.SeriesNumber);
        tags.add(Tag.PatientName);
        result = DATA_FILE_SET.getUniqueValuesFromFiles(files, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(3, result.size());
        for (int tag : result.keySet()) {
            assertEquals(1, result.get(tag).size());
        }
    }


    @Test
    public final void testGetUniqueValuesGivenValues() throws Exception {
        final Map<Integer, ConversionFailureException> failures = new HashMap<>();
        final Map<Integer, String>                     given    = new HashMap<>();
        final Set<Integer>                             tags     = new HashSet<>();
        SetMultimap<Integer, String>                   result;

        result = DATA_FILE_SET.getUniqueValuesGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(0, result.size());

        tags.add(Tag.SequenceName);
        result = DATA_FILE_SET.getUniqueValuesGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(1, result.keySet().size());
        assertEquals(numSequences, result.get(Tag.SequenceName).size());

        given.put(Tag.SequenceName, aSequenceName);
        tags.add(Tag.SeriesNumber);
        result = DATA_FILE_SET.getUniqueValuesGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(2, result.keySet().size());
        assertEquals(1, result.get(Tag.SequenceName).size());
        assertEquals(numSeriesInASequence, result.get(Tag.SeriesNumber).size());

        given.put(Tag.SeriesNumber, aSeriesNumber);
        tags.add(Tag.ImagePositionPatient);
        result = DATA_FILE_SET.getUniqueValuesGivenValues(given, tags, failures);
        assertTrue(failures.isEmpty());
        assertEquals(3, result.keySet().size());
        assertEquals(numInASequenceSeries, result.get(Tag.ImagePositionPatient).size());
    }

    @Test
    public final void testGetUniqueValuesInt() throws Exception {
        Set<String> values = DATA_FILE_SET.getUniqueValues(Tag.SequenceName);
        assertNotNull(values);
        assertEquals(numSequences, values.size());
    }

    @Test
    public final void testGetUniqueValuesSetOfInteger() throws Exception {
        final Map<Integer, ConversionFailureException> failures = new HashMap<>();
        final Set<Integer>                             tags     = new HashSet<>();
        tags.add(Tag.SOPInstanceUID);
        tags.add(Tag.ReferringPhysicianName);
        tags.add(Tag.SeriesNumber);
        final Multimap<Integer, String> values = DATA_FILE_SET.getUniqueValues(tags, failures);
        assertTrue(failures.isEmpty());

        assertNotNull(values);
        assertNotNull(values.get(Tag.SOPInstanceUID));
        assertEquals(numImageFiles, values.get(Tag.SOPInstanceUID).size());
        assertNotNull(values.get(Tag.ReferringPhysicianName));
        assertEquals(numReferringPhys, values.get(Tag.ReferringPhysicianName).size());
        assertNotNull(values.get(Tag.SeriesNumber));
        assertEquals(numSeries, values.get(Tag.SeriesNumber).size());

        // didn't ask for this info
        assertFalse(values.containsKey(Tag.ImageType));
    }

    @Test
    public final void testSize() throws Exception {
        final FileSet fileSet = new FileSet();
        try {
            assertEquals(0, fileSet.size());
            fileSet.add(DATA_FILE_1);
            assertEquals(1, fileSet.size());
            fileSet.add(DATA_FILE_2, DATA_FILE_3);
            assertEquals(3, fileSet.size());
            fileSet.remove(DATA_FILE_2);
            assertEquals(2, fileSet.size());
            fileSet.remove(DATA_FILE_1, DATA_FILE_3);
            assertEquals(0, fileSet.size());
        } finally {
            fileSet.dispose();
        }
    }

    @Test
    public final void testIsEmpty() throws Exception {
        final FileSet fileSet = new FileSet();
        try {
            assertTrue(fileSet.isEmpty());
            fileSet.add(DATA_FILE_1);
            assertFalse(fileSet.isEmpty());
            fileSet.add(DATA_FILE_2);
            assertFalse(fileSet.isEmpty());
            fileSet.remove(DATA_FILE_1, DATA_FILE_2);
            assertTrue(fileSet.isEmpty());
        } finally {
            fileSet.dispose();
        }
    }
}
