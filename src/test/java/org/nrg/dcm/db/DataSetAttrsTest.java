/*
 * Copyright (c) 2006-2010 Washington University
 */
package org.nrg.dcm.db;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;
import org.junit.Test;
import org.nrg.attr.ConversionFailureException;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import static org.junit.Assert.*;

public class DataSetAttrsTest {
    // Tests based on the sample1 dataset, with each file gzipped
    private static final File DATA_DIRECTORY = new File("src/test/resources/dicom");
    private static final File DATA_FILE      = new File(DATA_DIRECTORY, "1.MR.head_DHead.4.1.20061214.091206.156000.1632817982.dcm.gz");
    private static final File HEADLESS;

    static {
        try {
            HEADLESS = File.createTempFile("headless", ".dcm");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    final private String sequenceName = "*tfl3d1_ns";

    private static final String GZIP_SUFFIX = ".gz";

    private static DicomObject readPart10File(final File file) throws IOException {
        try (final DicomInputStream in = new DicomInputStream(new BufferedInputStream(DATA_FILE.getName().endsWith(GZIP_SUFFIX) ? new GZIPInputStream(new FileInputStream(file)) : new FileInputStream(file)))) {
            final DicomObject dicomObject = in.readDicomObject();
            if (dicomObject.contains(Tag.FileMetaInformationVersion)) {
                // looks part 10 compliant; assume it's valid
                return dicomObject;
            } else {
                throw new IOException(DATA_FILE.getPath() + " is not a valid DICOM file: no part 10 header");
            }
        }
    }

    private static void makeFMIlessCopy(final File dest, final File source) throws IOException {
        final DicomObject dicomObject = readPart10File(source);
        try (final DicomOutputStream dos = new DicomOutputStream(dest)) {
            dos.writeDataset(dicomObject, dicomObject.getString(Tag.TransferSyntaxUID));
        }
    }


    @Test
    public final void testDataSetAttrsFileInt() throws IOException {
        try {
            final DataSetAttrs dsa = new DataSetAttrs(DATA_FILE, Tag.SequenceName);
            assertNotNull(dsa);

            makeFMIlessCopy(HEADLESS, DATA_FILE);
            final DataSetAttrs hdsa = new DataSetAttrs(HEADLESS, Tag.SequenceName);
            assertNotNull(hdsa);
        } finally {
            HEADLESS.delete();
        }
    }

    @Test
    public final void testDataSetAttrsFileSetOfInteger() throws IOException {
        Set<Integer> attrs = new HashSet<>();

        // do it once with an empty attribute set...
        // empty attribute set is no longer allowed
        //    try {
        //      DataSetAttrs dsa = new DataSetAttrs(DATA_FILE, attrs);
        //      assertNotNull(dsa);
        //    } catch (IOException e) {
        //      fail("DataSetAttrs() generated an IOException: " + e);
        //    }

        attrs.add(Tag.SequenceName);
        attrs.add(Tag.AcquisitionDate);
        attrs.add(Tag.AcquisitionTime);
        attrs.add(Tag.OperatorsName);

        // and again with a nonempty set
        final DataSetAttrs dsa = new DataSetAttrs(DATA_FILE, attrs);
        assertNotNull(dsa);
    }

    @Test
    public final void testDataSetAttrsFile() throws IOException {
        DataSetAttrs dsa = new DataSetAttrs(DATA_FILE);
        assertNotNull(dsa);
    }

    @Test
    public final void testGet() throws IOException, ConversionFailureException {
        Set<Integer> attrs = new HashSet<>();
        attrs.add(Tag.SequenceName);

        // test some values without conversions,
        // and at least one value of each type requiring conversion
        final DataSetAttrs dsa = new DataSetAttrs(DATA_FILE, attrs);
        assertNotNull(dsa);
        assertEquals(sequenceName, dsa.get(Tag.SequenceName));
    }

    @Test
    public void testConvert() throws ConversionFailureException {
        final DicomObject o = new BasicDicomObject();
        o.putInt(Tag.DataElementsSigned, VR.AT, 0x00010001);
        o.putInts(Tag.FailureAttributes, VR.AT, new int[]{0x00020001, 0x00010003});
        assertEquals("(0001,0001)", DataSetAttrs.convert(o, Tag.DataElementsSigned));
        assertEquals("(0002,0001)\\(0001,0003)", DataSetAttrs.convert(o, Tag.FailureAttributes));
    }

    @Test
    public void testProblemVRs() throws IOException, ConversionFailureException {
        final DataSetAttrs dsa = new DataSetAttrs(DATA_FILE, Tag.PixelData - 1);
        assertNotNull(dsa);
        assertEquals(sequenceName, dsa.get(Tag.SequenceName));
        assertEquals("IMAGE NUM 4", dsa.get(0x00291008));    // Siemens-specific, UN VR
        //      assertEquals("(sequence)", dsa.get(Tag.IconImageSequence)); // SQ
        // Problem VR AT is tested in testConvert()
    }

    @Test
    public void testIterator() throws IOException {
        final DataSetAttrs dsa = new DataSetAttrs(DATA_FILE, Tag.PixelData - 1);
        assertNotNull(dsa);
        final Iterator<Integer> tagi = dsa.iterator();
        assertNotNull(tagi);
        assertTrue(tagi.hasNext());
        assertEquals(Integer.valueOf(Tag.SpecificCharacterSet), tagi.next());
        assertEquals(Integer.valueOf(Tag.ImageType), tagi.next());
        assertEquals(Integer.valueOf(Tag.InstanceCreationDate), tagi.next());
        assertEquals(Integer.valueOf(Tag.InstanceCreationTime), tagi.next());
        assertEquals(Integer.valueOf(Tag.SOPClassUID), tagi.next());
        // and on and on...
    }
}
