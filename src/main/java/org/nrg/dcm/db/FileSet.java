/*
 * Copyright (c) 2006-2010 Washington University
 */

package org.nrg.dcm.db;

import com.google.common.base.Joiner;
import com.google.common.collect.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.media.DicomDirReader;
import org.dcm4che2.net.TransferCapability;
import org.nrg.attr.ConversionFailureException;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Finds DICOM files in a directory and manages access to attribute values
 * held in those files.
 *
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
@SuppressWarnings( {"WeakerAccess", "SqlDialectInspection", "SqlNoDataSourceInspection"})
@Slf4j
public final class FileSet {
    public FileSet() throws IOException, SQLException {
        this(Collections.emptyList(), null, null, null);
    }

    public FileSet(final File file) throws IOException, SQLException {
        this(Collections.singletonList(file), null, null, null);
    }

    @SuppressWarnings("unused")
    public FileSet(final List<File> files, final Map<String, String> options) throws IOException, SQLException {
        this(files, options, null, null);
    }

    public FileSet(final File file, final Map<String, String> options) throws IOException, SQLException {
        this(Collections.singletonList(file), options, null, null);
    }

    /**
     * @param buildRecords true if directory entry records should be built using default types
     */
    public FileSet(final List<File> files, final Map<String, String> options, final boolean buildRecords, final ProgressMonitorI progressMonitor) throws IOException, SQLException {
        this(files, options, buildRecords ? DirectoryRecord.getDefaultFactory() : null, progressMonitor);
    }

    public FileSet(final List<File> files, final boolean buildRecords, final ProgressMonitorI progressMonitor) throws IOException, SQLException {
        this(files, null, buildRecords, progressMonitor);
    }

    @SuppressWarnings("unused")
    public FileSet(final File file, final boolean buildRecords, final ProgressMonitorI progressMonitor) throws IOException, SQLException {
        this(Collections.singletonList(file), null, buildRecords, progressMonitor);
    }

    @SuppressWarnings("unused")
    public FileSet(final File file, final boolean buildRecords) throws IOException, SQLException {
        this(Collections.singletonList(file), null, buildRecords, null);
    }

    @SuppressWarnings("unused")
    public FileSet(final File file, final Map<String, String> options, final DirectoryRecord.Factory recordFactory, final ProgressMonitorI progressMonitor) throws IOException, SQLException {
        this(Collections.singletonList(file), options, recordFactory, progressMonitor);
    }

    public FileSet(final File file, final Map<String, String> options, final DirectoryRecord.Factory recordFactory) throws IOException, SQLException {
        this(Collections.singletonList(file), options, recordFactory, null);
    }
    
    private List<DicomObject> dcmObjects = new ArrayList<DicomObject>();
    
    public List<DicomObject> getDcmObjects() {
        return dcmObjects;
    }
    /**
     * @param files            List of files and/or directories to be included in FileSet
     * @param submittedOptions map of option name and values; valid options are "table", which specifies
     *                         a qualifier (e.g., CACHED) to be added to the CREATE TABLE command; and all hsqldb options.
     *                         If you don't know what options to set, you probably don't need to set any.
     * @param recordFactory    Factory object for Directory Record Entries; if null, no record entries are created
     * @param progressMonitor  The progress monitor.
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    public FileSet(final List<File> files, final Map<String, String> submittedOptions, final DirectoryRecord.Factory recordFactory, final ProgressMonitorI progressMonitor) throws IOException, SQLException {
        _factory = recordFactory;

        _recordTags.add(Tag.SOPClassUID);            // need these two tags for transfer capabilities;
        _recordTags.add(Tag.TransferSyntaxUID);      // also, DataSetAttrs constructor needs at least one tag

        _patients = _factory == null ? null : new LinkedHashSet<>();
        if (_factory != null) {
            DirectoryRecord.Type.getTypes().forEach(type -> _recordTags.addAll(_factory.getSelectionKeys(type)));
        }

        final Map<String, String> options = ObjectUtils.defaultIfNull(submittedOptions, Collections.emptyMap());

        //  don't need this file, but we use the name
        final File databaseFolder = File.createTempFile(DB_PREFIX, DB_SUFFIX);
        databaseFolder.deleteOnExit(); // add delete on exit in case this thread dies

        final String databaseFolderPath = databaseFolder.getPath();

        final String user     = "sa";
        final String password = "";

        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        final String connectionUrl = "jdbc:hsqldb:file:" + databaseFolderPath + options.keySet().stream().map(option -> ";" + option + "=" + options.get(option)).collect(Collectors.joining());
        log.info("Opening database connection to URL: {}", connectionUrl);
        _connection = DriverManager.getConnection(connectionUrl, user, password);

        final String createTable = getCreateTableQuery(options);
        log.info("Preparing to initialize database with query: {}", createTable);
        try (final Statement statement = _connection.createStatement()) {
            statement.execute(createTable);
        }

        qCount = _connection.prepareStatement(SQL_COUNT);
        qGetOnePath = _connection.prepareStatement(SQL_GET_ONE_PATH);
        qGetAllPaths = _connection.prepareStatement(SQL_GET_ALL_PATHS);
        qPathTestRow = _connection.prepareStatement(SQL_PATH_TEST_ROW);
        qPathAddRow = _connection.prepareStatement(SQL_PATH_ADD_ROW);
        qPathDeleteRow = _connection.prepareStatement(SQL_PATH_DELETE_ROW);
        qTransferCapabilities = _connection.prepareStatement(SQL_TRANSFER_CAPABILITIES);

        if (!shouldRetainDatabase()) {
            log.info("Configuring database files to be deleted on exit based on setting of the '{}' property", PROP_RETAIN_DB);
            for (final String suffix : SUFFIXES) {
                final File file = new File(databaseFolderPath + suffix);
                DB_FILES.add(file);
                file.deleteOnExit();
            }
        }

        // findDataFiles() must come after DirectoryRecord factory configuration
        findDataFiles(files, progressMonitor);

        if (null != progressMonitor) {
            progressMonitor.close();
        }

        _shouldRetainDatabase.set(BooleanUtils.toBoolean(StringUtils.defaultIfBlank(System.getProperty(PROP_RETAIN_DB), "false")));
        log.debug("Set retain database to {}", shouldRetainDatabase());
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(final Object object) {
        try {
            return object instanceof FileSet && getDataFiles().equals(((FileSet) object).getDataFiles());
        } catch (SQLException e) {
            return this == object;
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        try {
            return getDataFiles().hashCode();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void add(final Collection<File> files, final ProgressMonitorI progressMonitor) throws IOException, SQLException {
        findDataFiles(files, progressMonitor);
    }

    public void add(final Collection<File> files) throws IOException, SQLException {
        add(files, null);
    }

    public void add(final File... files) throws IOException, SQLException {
        add(Arrays.asList(files), null);
    }

    /**
     * Remove the given Files from this fileset.
     *
     * @param fileSet File records to be removed from the fileset
     *
     * @return number of file records removed
     */
    public int remove(final File... fileSet) {
        final AtomicInteger removed = new AtomicInteger();
        for (final File file : fileSet) {
            try {
                final String path = file.getCanonicalFile().getPath();
                log.debug("Executing prepared query with parameter '{}': {}", path, SQL_PATH_DELETE_ROW);
                qPathDeleteRow.setString(1, path);
                removed.addAndGet(qPathDeleteRow.executeUpdate());
            } catch (SQLException | IOException e) {
                log.error("Unable to remove {} from file set", file, e);
            }
        }
        return removed.get();
    }


    /**
     * Remove the referenced files for the given directory records from this fileset.
     *
     * @param records Collection of directory records to be removed
     *
     * @return Files removed from the fileset
     */
    public Collection<File> remove(final Collection<DirectoryRecord> records) {
        final Set<String> paths = new LinkedHashSet<>();

        for (final DirectoryRecord patient : _patients) {
            paths.addAll(patient.purge(records));
        }

        final Set<File> removed = new HashSet<>();

        for (final String pathname : paths) {
            final File file = new File(pathname);
            try {
                log.debug("Executing prepared query with parameter '{}': {}", pathname, SQL_PATH_DELETE_ROW);
                qPathDeleteRow.setString(1, pathname);
                qPathDeleteRow.executeUpdate();
                removed.add(file);
            } catch (SQLException e) {
                log.error("Unable to remove " + file.getPath() + " from file set", e);
            }
        }

        return removed;
    }


    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void dispose() {
        try (final Statement statement = _connection.createStatement()) {
            statement.execute(shouldRetainDatabase() ? "SHUTDOWN IMMEDIATELY" : "SHUTDOWN");
            _connection.close();
        } catch (SQLException e) {
            log.error("Could not close database: " + e.getMessage());
        }
        for (final File file : DB_FILES) {
            file.delete();  // these should be deleted at exit, but we can do it now
        }
    }


    /**
     * Sets the maximum length of attribute values, in characters.
     * Any values longer than this are truncated, with a truncation message appended.
     *
     * @param m maximum length; must be greater than zero.
     */
    public void setMaxValueLength(final int m) {
        if (m <= 0) {
            throw new IllegalArgumentException("max attribute value length must be > 0");
        }
        maxValueLength = m;
    }

    /**
     * Specifies that the full, original length of attributes is to be used.
     */
    @SuppressWarnings("unused")
    public void setMaxValueLength() {
        maxValueLength = NO_MAX_VALUE;
    }

    /**
     * Sets the message appended to attribute values that have been truncated.
     *
     * @param msg message appended to values to indicate truncation
     */
    public void setTruncateFormat(final String msg) {
        valueTruncateFormat = msg;
    }


    /**
     * Get the root of the tree of directory record entries for this FileSet
     *
     * @return The root entry DirectoryRecord, or null if no DirectoryRecord factory was provided
     *     to the FileSet constructor.
     */
    public Set<DirectoryRecord> getPatientDirRecords() {
        return _patients == null ? null : Collections.unmodifiableSet(_patients);
    }


    /**
     * @return Set of all data files in this file set
     */
    public Set<File> getDataFiles() throws SQLException {
        log.debug("Executing prepared query: {}", SQL_GET_ALL_PATHS);
        try (final ResultSet results = qGetAllPaths.executeQuery()) {
            final Set<File> files = new LinkedHashSet<>();
            while (results.next()) {
                files.add(new File(results.getString(1)));
            }
            return files;
        }
    }


    /**
     * @return Number of data files in this file set
     */
    public int size() throws SQLException {
        log.debug("Executing prepared query: {}", SQL_COUNT);
        try (final ResultSet results = qCount.executeQuery()) {
            results.next();
            return results.getInt(1);
        }
    }

    /*
     * @return true if there are any data files in this file set
     * @throws SQLException
     */
    public boolean isEmpty() throws SQLException {
        log.debug("Executing prepared query: {}", SQL_GET_ONE_PATH);
        try (final ResultSet results = qGetOnePath.executeQuery()) {
            return !results.next();
        }
    }


    /**
     * Returns the capabilities required to transfer the files in this FileSet
     *
     * @param role Role of the caller in the transfer (TransferCapability.SCU or .SCP)
     *
     * @return array of TransferCapability
     */
    public List<TransferCapability> getTransferCapabilities(final String role) throws SQLException {
        log.debug("Executing prepared query: {}", SQL_TRANSFER_CAPABILITIES);
        try (final ResultSet results = qTransferCapabilities.executeQuery()) {
            return extractTransferCapabilities(role, results);
        }
    }


    public List<TransferCapability> getTransferCapabilities(final String role, final Collection<File> files) throws SQLException {
        final String query = SQL_TRANSFER_CAPABILITIES + " WHERE path IN ('" + StringUtils.join(files, "', '") + "')";
        log.debug("Preparing to execute transfer capabilities query: {}", query);
        try (final Statement statement = _connection.createStatement();
             final ResultSet results = statement.executeQuery(query)) {
            return extractTransferCapabilities(role, results);
        }
    }


    /**
     * @return Set of "roots" for this file set, i.e., all directories that were specified
     *     as containing this file set, plus the parent directories of all explicitly specified files
     */
    public Set<File> getRoots() {
        return Collections.unmodifiableSet(_roots);
    }

    /**
     * Read all the named attributes from all files in the fileset, and
     * cache the results.
     *
     * @param tags DICOM tags of all attributes of interest
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    public void fillCache(final Collection<Integer> tags, final Map<Integer, ConversionFailureException> failed, final ProgressMonitorI progressMonitor) throws IOException, SQLException {
        final Set<File> files = getDataFiles();
        if (progressMonitor != null) {
            progressMonitor.setMinimum(0);
            progressMonitor.setMaximum(files.size());
            progressMonitor.setProgress(0);
        }

        final Collection<Integer> uncached = tags.stream().filter(Objects::nonNull).filter(tag -> !isCacheComplete(tag)).collect(Collectors.toList());
        if (uncached.isEmpty()) {
            return;
        }

        // Now load the needed attributes
        final AtomicInteger progress = new AtomicInteger();
        for (final File file : files) {
            cache(file, uncached, failed);
            if (null != progressMonitor) {
                if (progressMonitor.isCanceled()) {
                    return;
                }
                progressMonitor.setProgress(progress.incrementAndGet());
            }
        }
        if (progressMonitor != null) {
            progressMonitor.close();
        }
    }


    public void fillCache(Collection<Integer> tags, final Map<Integer, ConversionFailureException> failed)
        throws IOException, SQLException {
        fillCache(tags, failed, null);
    }


    public void cache(final File file, final int minTag, final int maxTag, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        if (isCacheComplete(file, minTag, maxTag)) {
            return;
        }
        DataSetAttrs attrs = new DataSetAttrs(file, maxTag);
        dcmObjects.add(attrs.getDicomObject());
        cacheAttrs(file, attrs, failed);
        declareCacheComplete(file, minTag, maxTag);
    }

    /**
     * Read the named files, get all attributes in the indicated range,
     * and cache the results.
     *
     * @param files  to read; if null, use all files in this file set
     * @param maxTag largest DICOM tag to cache
     */
    public void fillCache(Collection<File> files, final int minTag, final int maxTag, final Map<Integer, ConversionFailureException> failed, final ProgressMonitorI progressMonitor) throws IOException, SQLException {
        final List<File> working = new ArrayList<>(ObjectUtils.defaultIfNull(files, getDataFiles()));

        if (progressMonitor != null) {
            progressMonitor.setMinimum(0);
            progressMonitor.setMaximum(working.size());
            progressMonitor.setProgress(0);
        }

        int progress = 0;
        for (final File file : working) {
            cache(file, minTag, maxTag, failed);
            if (progressMonitor != null) {
                if (progressMonitor.isCanceled()) {
                    return;
                }
                progressMonitor.setProgress(++progress);
            }
        }
        if (progressMonitor != null) {
            progressMonitor.close();
        }
    }

    public void fillCache(Collection<File> files, final int minTag, final int maxTag, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        fillCache(files, minTag, maxTag, failed, null);
    }


    /**
     * Finds all files in the fileset that match all the given attribute values
     *
     * @param values map of DICOM tag to attribute value
     *
     * @return set of files matching all the given attribute values
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    public Set<File> getFilesForValues(final Map<Integer, String> values, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        if (values.isEmpty()) {       // special case: no constraints means all files
            return getDataFiles();
        }

        // First load all the requested attributes
        fillCache(values.keySet(), failed);

        final String query = "SELECT path FROM Attributes" + getWhereClause(values);
        log.debug("Preparing to query: {}", query);
        try (final Statement statement = _connection.createStatement();
             final ResultSet results = statement.executeQuery(query)) {
            final Set<File> files = new LinkedHashSet<>();
            while (results.next()) {
                final String path = results.getString(1);
                if (results.wasNull()) {
                    throw new IOException("NULL path value in cache");
                }
                assert path != null;
                files.add(new File(path));
            }
            return files;
        }
    }

    /**
     * Returns all combinations of values for the given attributes in the file set.
     *
     * @param tags tags for the attributes of interest
     *
     * @return A Set of tag->value Maps, each Map describing one combination.
     */
    public Set<Map<Integer, String>> getUniqueCombinations(final Collection<Integer> tags, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        return getUniqueCombinationsGivenValues(new HashMap<>(), tags, failed);
    }

    /**
     * Gets unique combinations of values for the specified tags.
     *
     * @param failed An empty mutable map in which this method returns any failures that occur while processing the tags.
     * @param tags   The tags for DICOM attributes to be retrieved.
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     * @see #getUniqueCombinations(Collection, Map)
     */
    @SuppressWarnings("unused")
    public Set<Map<Integer, String>> getUniqueCombinations(final Map<Integer, ConversionFailureException> failed, final Integer... tags) throws IOException, SQLException {
        return getUniqueCombinationsGivenValues(new HashMap<>(), Arrays.asList(tags), failed);
    }

    /**
     * Return all unique combinations of values in the fileset for both
     * the given and requested attributes, given the constrains.
     *
     * @param given     map of tag->value constraints
     * @param requested set of tags of requested attributes
     *
     * @return set of maps, each map holding a tag->value combination
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    public Set<Map<Integer, String>> getUniqueCombinationsGivenValues(final Map<Integer, String> given, final Collection<Integer> requested, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        if (requested.isEmpty()) {
            return Collections.emptySet();
        }

        // All the given are presumably already cached.
        // (apparently I'm not ready to assert this yet)
        final Set<Integer> allTags = new HashSet<>(requested);
        allTags.addAll(given.keySet());
        fillCache(allTags, failed);

        final SortedSet<Integer> tags = new TreeSet<>(requested);

        final String query = "SELECT DISTINCT " + StringUtils.join(tags.stream().map(_tagToColumn::get).collect(Collectors.toList()), ", ") + " FROM Attributes" + getWhereClause(given);

        log.debug("Executing unique combinations from values query: {}", query);
        final Set<Map<Integer, String>> combs = new HashSet<>();
        try (final Statement statement = _connection.createStatement();
             final ResultSet results = statement.executeQuery(query)) {
            while (results.next()) {
                final Map<Integer, String> vals = new HashMap<>();
                int                        i    = 1;
                for (final int tag : tags) {
                    final String val = results.getString(i++);
                    if (val != null) {
                        vals.put(tag, val);
                    }
                }
                combs.add(vals);
            }
        }

        return combs;
    }


    /**
     * Returns all values for the given attribute in the file set.
     *
     * @param tag tag for attribute of interest
     *
     * @return All values for the indicated attribute in the file set
     *
     * @throws IOException                When an error occurs reading or writing data from streams.
     * @throws ConversionFailureException When an error occurs trying to retrieve values for a DICOM tag.
     * @throws SQLException               When an error occurs accessing the database.
     */
    public Set<String> getUniqueValues(final int tag) throws IOException, ConversionFailureException, SQLException {
        final Map<Integer, ConversionFailureException> failed = new HashMap<>();
        final SetMultimap<Integer, String>             values = getUniqueValues(Collections.singleton(tag), failed);
        if (failed.isEmpty()) {
            return values.get(tag);
        } else {
            throw failed.get(tag);
        }
    }


    /**
     * Returns all values for the given attributes in the file set
     *
     * @param tags   The DICOM tags for the requested attributes
     * @param failed An empty mutable map in which this method returns any failures that occur while processing the tags.
     *
     * @return All values for the given attributes in this file set
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    public SetMultimap<Integer, String> getUniqueValues(final Collection<Integer> tags, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        final SetMultimap<Integer, String> values = HashMultimap.create();

        // First load all the requested attributes
        fillCache(tags, failed);

        try (final Statement statement = _connection.createStatement()) {
            for (final int tag : tags) {
                final String query = "SELECT DISTINCT " + _tagToColumn.get(tag) + " FROM Attributes";
                log.debug("Preparing to execute query: {}", query);
                try (final ResultSet results = statement.executeQuery(query)) {
                    while (results.next()) {
                        values.put(tag, results.getString(1));
                    }
                }
            }
            return values;
        } catch (SQLException e) {
            throw new IOException("error accessing hsqldb: " + e.getMessage());
        }
    }


    /**
     * Returns all values for the given attributes in the file set
     *
     * @param tags   The DICOM tags for the requested attributes
     * @param failed An empty mutable map in which this method returns any failures that occur while processing the tags.
     *
     * @return All values for the given attributes in this file set
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    @SuppressWarnings("unused")
    public SetMultimap<Integer, String> getUniqueValues(final Map<Integer, ConversionFailureException> failed, final Integer... tags) throws IOException, SQLException {
        return getUniqueValues(Arrays.asList(tags), failed);
    }

    /**
     * Returns all unique values for the requested attributes in the named files
     *
     * @param files Set of files from which attribute values are requested
     * @param tags  DICOM tags of attributes to be examined
     *
     * @return Map from DICOM tag to set of values
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    public SetMultimap<Integer, String> getUniqueValuesFromFiles(final Set<File> files, final Collection<Integer> tags, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        return getUniqueValuesFromFiles(files, tags, failed, false);
    }

    /**
     * Returns all values for the requested attributes in files for which the
     * given attributes have the given values.
     *
     * @param given  Map of attribute values used as constraint
     * @param tags   Set of tags for which we want values
     * @param failed An empty mutable map in which this method returns any failures that occur while processing the tags.
     *
     * @return Map of requested attribute values, given the constraint
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    public SetMultimap<Integer, String> getUniqueValuesGivenValues(final Map<Integer, String> given, final Set<Integer> tags, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        // All the given are presumably already cached.
        // (apparently I'm not ready to assert this yet)
        fillCache(Sets.union(tags, given.keySet()), failed);
        return getUniqueValuesFromFiles(getFilesForValues(given, failed), tags, failed, true);
    }


    /**
     * Returns values for all attributes in the given tag range in the given file
     *
     * @param file   The file from which values should be retrieved.
     * @param minTag Minimum tag to return
     * @param maxTag Maximum tag to return
     *
     * @return A map of tags and values from the specified file.
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    public Map<Integer, String> getValuesFromFile(final File file, final int minTag, final int maxTag, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        final Map<Integer, String> values = new HashMap<>();

        cache(file, minTag, maxTag, failed);

        // convert the range to an ordered set of tags
        final SortedSet<Integer> tags = new TreeSet<>();
        for (final int tag : _tagToColumn.keySet()) {
            if (minTag <= tag && tag <= maxTag) {
                tags.add(tag);
            }
        }

        final String query = "SELECT " + StringUtils.join(tags.stream().filter(i -> minTag <= i && i <= maxTag).collect(Collectors.toList()).stream().map(_tagToColumn::get).collect(Collectors.toList()), ", ") + " FROM Attributes WHERE path = '" + file.getPath() + "'";

        log.debug("Executing query to get values from a particular file: {}", query);
        try (final Statement statement = _connection.createStatement();
             final ResultSet results = statement.executeQuery(query)) {
            if (results.next()) {
                int col = 1;
                for (final int tag : tags) {    // iteration order must be same as above
                    values.put(tag, results.getString(col++));
                }
                if (results.next()) {
                    log.warn("Multiple rows found for {}", file.getPath());
                }
            } else {
                log.error("{} was cached but is missing from database", file.getPath());
            }
        } catch (SQLException e) {
            throw new IOException("Error accessing hsqldb", e);
        }
        return values;
    }


    /**
     * Returns all unique values for all attributes in the given tag range in the
     * named files, after ensuring that the values are cached.
     *
     * @param files           Files to look for values
     * @param minTag          Minimum tag to return
     * @param maxTag          Maximum tag to return
     * @param progressMonitor {@link ProgressMonitorI Progress monitor object} to monitor progress
     *
     * @return A map of tags and values from the specified files.
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    public Map<Integer, Set<String>> getUniqueValuesFromFiles(final Set<File> files, final int minTag, final int maxTag, final Map<Integer, ConversionFailureException> failed, final ProgressMonitorI progressMonitor) throws IOException, SQLException {
        int progress = 0;
        if (progressMonitor != null) {
            progressMonitor.setMinimum(0);
            progressMonitor.setProgress(progress);
            progressMonitor.setMaximum(files.size());
        }

        final Map<Integer, Set<String>> vals = new HashMap<>();
        for (final File file : files) {
            final Map<Integer, String> fv = getValuesFromFile(file, minTag, maxTag, failed);
            for (Map.Entry<Integer, String> e : fv.entrySet()) {
                final int tag = e.getKey();
                if (!vals.containsKey(tag)) {
                    vals.put(tag, new HashSet<>());
                }
                vals.get(tag).add(e.getValue());
            }
            if (progressMonitor != null) {
                progressMonitor.setProgress(++progress);
            }
        }

        return vals;
    }


    /**
     * Returns all unique values for all attributes in the given tag range in the
     * named files, after ensuring that the values are cached.
     *
     * @param files  Files to look for values
     * @param minTag Minimum tag to return
     * @param maxTag Maximum tag to return
     *
     * @return A map of tags and values from the specified files.
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    @SuppressWarnings("unused")
    public Map<Integer, Set<String>> getUniqueValuesFromFiles(final Set<File> files, final int minTag, final int maxTag, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        fillCache(files, minTag, maxTag, failed, null);
        return getUniqueValuesFromFiles(files, minTag, maxTag, failed, null);
    }

    /**
     * Indicates whether the specified tag is a sequence.
     *
     * @param tag The tag to check.
     *
     * @return Returns true if the tag is in the available tag set and is a sequence.
     */
    public boolean isSequenceTag(final int tag) {
        return _tagToColumn.containsKey(tag) && _tagToColumn.get(tag).startsWith("SQ")||dcmObjects.get(0).contains(tag)&&dcmObjects.get(0).get(tag).toString().contains("SQ");    }

    private boolean shouldRetainDatabase() {
        return _shouldRetainDatabase.get();
    }

    private String getCreateTableQuery(final Map<String, String> options) {
        final StringBuilder createTable = new StringBuilder("CREATE ");
        if (options.containsKey(CREATE_TABLE_OPTION)) {
            createTable.append(options.get(CREATE_TABLE_OPTION));
            createTable.append(" ");
            options.remove(CREATE_TABLE_OPTION);      // don't pass this to hsqldb
        }
        createTable.append("TABLE Attributes (path VARCHAR PRIMARY KEY, sop_class_uid VARCHAR, transfer_syntax_uid VARCHAR)");
        return createTable.toString();
    }

    private boolean contains(@SuppressWarnings("unused") final File file) throws SQLException {
        final String path = file.getPath();
        qPathTestRow.setString(1, path);
        log.debug("Executing prepared query with parameter '{}': {}", path, SQL_PATH_TEST_ROW);
        try (final ResultSet results = qPathTestRow.executeQuery()) {
            return results.next();
        }
    }

    private static List<TransferCapability> extractTransferCapabilities(final String role, final ResultSet results) throws SQLException {
        final Multimap<String, String> transferCapabilityMap = LinkedHashMultimap.create();
        while (results.next()) {
            transferCapabilityMap.put(results.getString(1), results.getString(2));
        }

        return transferCapabilityMap.keySet().stream().map(key -> new TransferCapability(key, transferCapabilityMap.get(key).toArray(new String[0]), role)).collect(Collectors.toList());
    }

    /**
     * Walks the entries of a DICOMDIR file set to extract the image file information.
     */
    private synchronized Collection<File> readFileSetRecords(final File dir, final DicomDirReader dicomDirReader, final DicomObject fsRecord, final DirectoryRecord upper) throws IOException {
        final List<File> files = new ArrayList<>();

        for (DicomObject record = fsRecord; record != null; record = dicomDirReader.findNextSiblingRecord(record)) {
            try {
                DirectoryRecord            rec  = null;
                final DirectoryRecord.Type type = DirectoryRecord.Type.getInstance(record.getString(Tag.DirectoryRecordType));
                if (type == null) {
                    log.warn("Unrecognized DICOMDIR record type " + record.getString(Tag.DirectoryRecordType));
                    continue;
                }

                String path = null;
                if (record.contains(Tag.ReferencedFileID)) {
                    // dcm4che breaks fields on the same character used as pathname separator ('\')
                    // this isn't such a bad thing, because we want to use the os-appropriate separator anyway
                    final File file = new File(dir, StringUtils.join(record.getStrings(Tag.ReferencedFileID), File.separatorChar)).getCanonicalFile();
                    assert file.getCanonicalPath().equals(file.getPath());
                    if (file.exists()) {
                        path = file.getPath();
                        if (DirectoryRecord.Type.INSTANCE.equals(type)) {
                            files.add(file);
                        }
                    }
                }

                if (_factory != null) {
                    // Extract the available information from the DICOMDIR
                    final Map<Integer, String> values = new HashMap<>();
                    for (final int tag : _factory.getSelectionKeys(type)) {
                        values.put(tag, (tag == Tag.ReferencedFileID) ? path : record.getString(tag));
                    }

                    rec = _factory.newInstance(type, upper, values);

                    if (DirectoryRecord.Type.PATIENT.equals(type)) {
                        if (upper != null) {
                            throw new IOException("PATIENT directory record entry not at top level in DICOMDIR");
                        }
                        _patients.add(rec);
                    } else if (upper == null) {
                        throw new IOException(type + " directory record entry without upper level in DICOMDIR");
                    }
                }

                final DicomObject c = dicomDirReader.findFirstChildRecord(record);
                if (c != null) {
                    files.addAll(readFileSetRecords(dir, dicomDirReader, c, rec));
                }
            } catch (IOException e) {    // IOException is bad news for our ability to continue
                throw e;
            } catch (Exception e) {    // Other exceptions are troubling but not necessarily disasters
                log.error("Error reading DICOMDIR {} directory record: {}\nRecord object: {}", dir.getPath(), e.getMessage(), record);
            }
        }

        return files;
    }


    /**
     * Walks the given files to find DICOM files.
     * If a directory contains a DICOMDIR, we stop descending that way
     * and assume that the DICOMDIR catalogs all DICOM files.
     *
     * @param incomingFiles   Incoming files.
     * @param progressMonitor The progress monitor.
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    private synchronized void findDataFiles(final Collection<File> incomingFiles, final ProgressMonitorI progressMonitor) throws IOException, SQLException {
        final Set<File>  directories = new LinkedHashSet<>();
        final List<File> files       = new ArrayList<>();
        int              progress    = 0;

        if (progressMonitor != null) {
            progressMonitor.setMinimum(0);
            progressMonitor.setMaximum(incomingFiles.size());
            progressMonitor.setProgress(progress);
            progressMonitor.setNote("Scanning directories...");    // TODO: localize
        }

        for (final File file : incomingFiles) {
            final File canonicalFile = file.getCanonicalFile();
            if (canonicalFile.isDirectory()) {
                directories.add(canonicalFile);
                _roots.add(canonicalFile);
            } else if (DICOMDIR.equals(canonicalFile.getName())) {
                // If this is a DICOMDIR, it indexes lots of files.
                final DicomDirReader dcd = new DicomDirReader(canonicalFile);
                final File           dir = canonicalFile.getParentFile();
                files.addAll(readFileSetRecords(dir, dcd, dcd.findFirstRootRecord(), null));
                dcd.close();
                _roots.add(dir);
            } else {
                files.add(canonicalFile);
                _roots.add(canonicalFile.getParentFile());
            }
            if (progressMonitor != null) {
                progressMonitor.setMaximum(directories.size() + files.size());
                progressMonitor.setProgress(progress);
            }
        }

        // Walk through the given directories and all their subdirectories.
        // Because directories is a LinkedHashSet, the iterator is FIFO (like a Queue),
        // but add()ing a directory that's already present doesn't add a second instance.
        for (Iterator<File> di = directories.iterator(); di.hasNext(); ) {
            final File dir = di.next();
            di.remove();
            assert dir.isDirectory();
            assert dir.getPath().equals(dir.getCanonicalPath());    // we add only canonical paths

            if (progressMonitor != null) {
                progressMonitor.setNote(dir.getPath());
            }

            boolean dirsAdded = false;

            final File dicomDir = new File(dir, DICOMDIR);
            if (dicomDir.exists()) {
                // If there's a DICOMDIR in this directory, assume that it indexes all files
                // in this directory and its subdirectories.
                final DicomDirReader dcd = new DicomDirReader(dicomDir);
                files.addAll(readFileSetRecords(dir, dcd, dcd.findFirstRootRecord(), null));
                dcd.close();
            } else {
                final File[] contents = dir.listFiles();
                if (contents != null) {
                    for (final File file : contents) {
                        final File canonicalFile = file.getCanonicalFile();
                        if (file.isDirectory()) {
                            // Try not to follow symbolic links (this is an imperfect kludge)
                            if (canonicalFile.getPath().equals(file.getAbsolutePath())) {
                                directories.add(canonicalFile);
                                dirsAdded = true;
                            }
                        } else {
                            files.add(canonicalFile);
                        }
                    }
                }

                if (progressMonitor != null) {
                    progressMonitor.setMaximum(directories.size() + files.size());
                    progressMonitor.setProgress(progress);
                }

                // If we've added any directories, need to rebuild the iterator.
                if (dirsAdded) {
                    di = directories.iterator();
                }
            }
        }

        if (progressMonitor != null) {
            progressMonitor.setMaximum(files.size());
        }

        for (final File file : files) {
            final String path = file.getPath();
            if (progressMonitor != null) {
                if (progressMonitor.isCanceled()) {
                    return;
                }
                progressMonitor.setNote(path);
            }

            assert !file.isDirectory();
            assert path.equals(file.getCanonicalPath());    // we add only canonical paths

            // Don't read a file more than once.
            if (contains(file)) {
                if (progressMonitor != null) {
                    progressMonitor.setProgress(++progress);
                }
                continue;
            }

            // Try reading the file as a DICOM object.
            final DataSetAttrs attrs;
            try {
                attrs = new DataSetAttrs(file, _recordTags);
            } catch (IOException e) {
                if (progressMonitor != null) {
                    progressMonitor.setProgress(++progress);
                }
                continue;   // not a DICOM object; move on to next file
            }

            // Add this file to the db
            final String sopClassUid       = attrs.getString(Tag.SOPClassUID);
            final String transferSyntaxUid = attrs.getString(Tag.TransferSyntaxUID, DEFAULT_TS_UID);

            log.debug("Executing prepared query with parameters path['{}'], sopClassUid['{}'], transferSyntaxUid['{}']: {}", path, sopClassUid, transferSyntaxUid, SQL_PATH_ADD_ROW);
            qPathAddRow.setString(1, path);
            qPathAddRow.setString(2, sopClassUid);
            qPathAddRow.setString(3, transferSyntaxUid);
            final int updated = qPathAddRow.executeUpdate();
            assert 1 == updated;

            if (_factory != null) {
                // build a Patient record; if a match already exists, use that instead.
                final Map<Integer, String> patientValues = new HashMap<>();
                for (final int tag : _factory.getSelectionKeys(DirectoryRecord.Type.PATIENT)) {
                    try {
                        patientValues.put(tag, attrs.get(tag));
                    } catch (ConversionFailureException ignored) {
                    }
                }

                final DirectoryRecord patient = getDirectoryRecord(_patients, DirectoryRecord.Type.PATIENT, patientValues);
                _patients.add(patient);

                // build the Study; we don't just set the parent in the constructor because
                // we might reuse an older instance instead, in which case this Study object
                // will be discarded, and we don't want the Patient to have a reference to it.
                final Map<Integer, String> studyValues = new HashMap<>();
                for (final int tag : _factory.getSelectionKeys(DirectoryRecord.Type.STUDY)) {
                    try {
                        studyValues.put(tag, attrs.get(tag));
                    } catch (ConversionFailureException ignored) {
                    }
                }

                final DirectoryRecord study = getDirectoryRecord(patient, DirectoryRecord.Type.STUDY, studyValues);

                // build the Series (no Study in constructor: see note for Study above)
                final Map<Integer, String> seriesValues = new HashMap<>();
                for (final int tag : _factory.getSelectionKeys(DirectoryRecord.Type.SERIES)) {
                    try {
                        seriesValues.put(tag, attrs.get(tag));
                    } catch (ConversionFailureException ignored) {
                    }
                }

                final DirectoryRecord series = getDirectoryRecord(study, DirectoryRecord.Type.SERIES, seriesValues);

                // finally, build the Image (no Series in constructor: see note for Study above)
                final Map<Integer, String> imageValues = new HashMap<>();
                for (int tag : _factory.getSelectionKeys(DirectoryRecord.Type.INSTANCE)) {
                    try {
                        imageValues.put(tag, attrs.get(tag));
                    } catch (ConversionFailureException ignored) {
                    }
                }
                imageValues.put(Tag.ReferencedFileID, path);

                getDirectoryRecord(series, DirectoryRecord.Type.INSTANCE, imageValues);
            }

            if (progressMonitor != null) {
                progressMonitor.setProgress(++progress);
            }
        }
    }

    /**
     * Creates a new {@link DirectoryRecord directory record instance} from the specified type and values. If the collection of existing records contains
     * an instance that matches the newly created instance, the existing instance is returned. If not, the newly created instance is returned instead.
     *
     * @param records    A collection of existing records.
     * @param recordType The type of record to retrieve
     * @param values     The values with which to populate the record.
     *
     * @return Returns a {@link DirectoryRecord directory record instance} from the collection of existing records or a newly created record.
     */
    private DirectoryRecord getDirectoryRecord(final Collection<DirectoryRecord> records, final DirectoryRecord.Type recordType, final Map<Integer, String> values) {
        final List<DirectoryRecord> recordsList = new ArrayList<>(records);
        final DirectoryRecord       record      = _factory.newInstance(recordType, null, values);
        return records.contains(record) ? recordsList.get(recordsList.indexOf(record)) : record;
    }

    private DirectoryRecord getDirectoryRecord(final DirectoryRecord upper, final DirectoryRecord.Type recordType, final Map<Integer, String> values) {
        final DirectoryRecord located = getDirectoryRecord(upper.getLower(), recordType, values);
        located.setUpper(upper);
        return located;
    }

    private boolean isCacheComplete(final int tag) {
        try {
            try (final Statement statement = _connection.createStatement()) {
                final String col = _tagToColumn.get(tag);
                if (col == null) {
                    return false;
                }
                final String query = "SELECT COUNT(" + col + ") FROM Attributes";
                log.debug("Executing query: {}", query);
                try (final ResultSet results = statement.executeQuery(query)) {
                    results.next();
                    final int count = results.getInt(1);
                    final int size  = size();
                    assert count <= size : "too many cache entries: found " + count + ", expected <= " + size;
                    return count == size;
                }
            }
        } catch (SQLException e) {
            log.error("SQL exception while checking cache completeness: " + e.getMessage());
            return false;
        }

    }

    private boolean isCacheComplete(final File file, final Integer... tags) {
        final int[] tagRange   = getTagRange(tags);
        final int   minimumTag = tagRange[0];
        final int   maximumTag = tagRange[1];
        if (_declaredComplete.containsKey(file)) {
            final int[] completeRange = _declaredComplete.get(file);
            return completeRange[0] <= minimumTag && maximumTag <= completeRange[1];
        }
        return false;
    }


    /**
     * Check to see if this file has already been declared complete for
     * some range; if so, and the old and new ranges overlap, we can
     * declare the union complete (rather than just the new).
     * If the ranges don't overlap, just assign the new.
     *
     * @param file File for which we are declaring completeness
     * @param tags minimum and (optionally) maximum tags
     */
    private synchronized void declareCacheComplete(final File file, final Integer... tags) {
        final int[] tagRange   = getTagRange(tags);
        final int   minimumTag = tagRange[0];
        final int   maximumTag = tagRange[1];
        if (_declaredComplete.containsKey(file)) {
            final int[] prevRange = _declaredComplete.get(file);
            if (prevRange[0] < minimumTag && minimumTag <= prevRange[1]) {
                tagRange[0] = prevRange[0];
            }
            if (prevRange[0] <= maximumTag && tagRange[1] <= prevRange[1]) {
                tagRange[1] = prevRange[1];
            }
        }
        _declaredComplete.put(file, tagRange);
    }

    private int[] getTagRange(final Integer... tags) {
        assert tags.length == 1 || tags.length == 2;
        return new int[] {tags[0], tags.length == 2 ? tags[1] : tags[0]};
    }

    private void cacheAttrs(final File file, final DataSetAttrs attrs, final Map<Integer, ConversionFailureException> failed) throws SQLException {
        final String path = file.getPath();
        try {
            assert path.equals(file.getCanonicalPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        for (final int tag : attrs) {
            if (!_tagToColumn.containsKey(tag)) {      // no column for this attribute; add one.
                String col = String.format(COLUMN_FORMAT, attrs.getVR(tag), tag);
                _tagToColumn.put(tag, col);
                try (final Statement statement = _connection.createStatement()) {
                    final String query = "ALTER TABLE Attributes ADD COLUMN " + col + " VARCHAR";
                    log.debug("Executing query: {}", query);
                    statement.execute(query);
                    for (SQLWarning warning = statement.getWarnings(); warning != null; warning = warning.getNextWarning()) {
                        log.warn(warning.toString());
                    }
                }
            }
        }

        final StringBuilder query = new StringBuilder("UPDATE Attributes SET ");

        final Set<String> assignments = new HashSet<>();
        for (final int tag : attrs) {
            try {
                final String value = attrs.get(tag);
                if (StringUtils.isNotBlank(value)) {
                    final String formatted = 0 < maxValueLength && maxValueLength < value.length() ? String.format(valueTruncateFormat, value.substring(0, maxValueLength)) : value;
                    assignments.add(String.format("%1$s='%2$s'", _tagToColumn.get(tag), StringUtils.replace(formatted, "'", "''")));
                }
            } catch (ConversionFailureException e) {
                failed.put(tag, e);
            }
        }

        if (assignments.isEmpty()) {
            return;
        }

        Joiner.on(", ").appendTo(query, assignments);
        query.append(" WHERE path='").append(path).append("'");

        try (final Statement statement = _connection.createStatement()) {
            log.debug("Executing query: {}", query);
            final int updated = statement.executeUpdate(query.toString());
            assert 1 == updated;
        }
    }

    private void cache(final File file, final Collection<Integer> tags, final Map<Integer, ConversionFailureException> failed) throws IOException, SQLException {
        cacheAttrs(file, new DataSetAttrs(file, tags.stream().filter(i -> !isCacheComplete(file, i)).collect(Collectors.toList())), failed);
    }

    private String getWhereClause(final Map<Integer, String> values) {
        if (values == null || values.isEmpty()) {
            return "";
        }

        return " WHERE " + StringUtils.join(values.keySet().stream().map(value -> String.format("%1$s='%2$s'", _tagToColumn.get(value), StringUtils.replace(values.get(value), "'", "''"))).collect(Collectors.toList()), " AND ");
    }

    /**
     * Return all unique values for the requested attributes in the given files.
     *
     * @param files              Set of files from which attribute values are requested
     * @param cacheKnownComplete true if the cache is known to be complete for
     *                           these attributes in these files (e.g., if we just filled the cache)
     *
     * @return Map from DICOM tag to set of values
     *
     * @throws IOException  When an error occurs reading or writing data from streams.
     * @throws SQLException When an error occurs accessing the database.
     */
    private SetMultimap<Integer, String> getUniqueValuesFromFiles(final Set<File> files, final Collection<Integer> requested, final Map<Integer, ConversionFailureException> failed, final boolean cacheKnownComplete) throws IOException, SQLException {
        final SetMultimap<Integer, String> values = HashMultimap.create();
        if (files.isEmpty() || requested.isEmpty()) {
            return values;
        }

        // make a sorted local copy to ensure iteration order and to get min and max
        final SortedSet<Integer> tags = new TreeSet<>(requested);
        if (!cacheKnownComplete) {
            fillCache(files, tags.first(), tags.last(), failed);
        }

        final String query = "SELECT path, " + StringUtils.join(tags.stream().map(_tagToColumn::get).collect(Collectors.toList()), ", ") + " FROM Attributes";

        log.debug("Preparing to execute query: {}", query);
        try (final Statement statement = _connection.createStatement();
             final ResultSet results = statement.executeQuery(query)) {
            while (results.next()) {
                final File file = new File(results.getString(1));
                if (files.contains(file)) {
                    int i = 2;
                    for (final int tag : tags) {
                        values.put(tag, results.getString(i++));
                    }
                }
            }
        }

        return values;
    }


    private static final List<String> SUFFIXES                  = Arrays.asList("", ".properties", ".script", ".data", ".backup", ".log", ".lck");
    private static final String       SQL_COUNT                 = "SELECT COUNT(*) FROM Attributes";
    private static final String       SQL_GET_ONE_PATH          = "SELECT TOP 1 path FROM Attributes";
    private static final String       SQL_GET_ALL_PATHS         = "SELECT path FROM Attributes";
    private static final String       SQL_PATH_TEST_ROW         = "SELECT path FROM Attributes WHERE path = ?";
    private static final String       SQL_PATH_ADD_ROW          = "INSERT INTO Attributes (path, sop_class_uid, transfer_syntax_uid) VALUES (?, ?, ?)";
    private static final String       SQL_PATH_DELETE_ROW       = "DELETE FROM Attributes WHERE path = ?";
    private static final String       SQL_TRANSFER_CAPABILITIES = "SELECT DISTINCT sop_class_uid, transfer_syntax_uid FROM Attributes";
    private static final String       PROP_RETAIN_DB            = "org.nrg.DicomDB.retain-db";
    private static final String       DICOMDIR                  = "DICOMDIR";
    private static final String       DB_PREFIX                 = "org.nrg.dcm";
    private static final String       DB_SUFFIX                 = "-db";
    private static final List<File>   DB_FILES                  = new ArrayList<>();
    private static final String       CREATE_TABLE_OPTION       = "table";
    private static final int          NO_MAX_VALUE              = -1;

    private static int maxValueLength = NO_MAX_VALUE;

    // Default Transfer Syntax UID (PS 3.5, section 10)
    private static final String DEFAULT_TS_UID = UID.ImplicitVRLittleEndian;

    // String format used to construct column names from tags
    private static final String COLUMN_FORMAT = "%1$s%2$08x";

    private final AtomicBoolean              _shouldRetainDatabase = new AtomicBoolean();
    private final SortedSet<Integer>         _recordTags           = new TreeSet<>();
    private final Map<File, int[]>           _declaredComplete     = new HashMap<>();
    private final SortedMap<Integer, String> _tagToColumn          = new TreeMap<>();
    private final Set<File>                  _roots                = new LinkedHashSet<>();

    private final DirectoryRecord.Factory _factory;
    private final Set<DirectoryRecord>    _patients;
    private final Connection              _connection;
    private final PreparedStatement       qCount, qGetAllPaths, qGetOnePath, qPathTestRow, qPathAddRow, qPathDeleteRow, qTransferCapabilities;

    private String valueTruncateFormat = "%1$s...(truncated)";
}
