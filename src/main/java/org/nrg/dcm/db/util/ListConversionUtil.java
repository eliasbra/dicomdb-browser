package org.nrg.dcm.db.util;

import java.util.*;

public class ListConversionUtil {
    public static <T> List<T> toList(final Iterable<T> iterable) {
        return toList(iterable, DEFAULT_ESTIMATED_SIZE);
    }

    public static <T> List<T> toList(final Iterable<T> iterable, int estimatedSize) {
        validateParameters(iterable, estimatedSize);
        final Iterator<T> iterator = iterable.iterator();
        final List<T>     list     = new ArrayList<>(estimatedSize);
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }

    public static <T> Set<T> toSet(final Iterable<T> iterable) {
        return toSet(iterable, DEFAULT_ESTIMATED_SIZE);
    }

    public static <T> Set<T> toSet(final Iterable<T> iterable, int estimatedSize) {
        validateParameters(iterable, estimatedSize);
        final Iterator<T> iterator = iterable.iterator();
        final Set<T>      list     = new HashSet<>(estimatedSize);
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }

    public static <T> SortedSet<T> toSortedSet(final Iterable<T> iterable) {
        return toSortedSet(iterable.iterator());
    }

    public static <T> SortedSet<T> toSortedSet(final Iterator<T> iterator) {
        validateParameters(iterator, 1);
        final SortedSet<T> list = new TreeSet<>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }

    private static void validateParameters(final Object iterable, final int estimatedSize) {
        if (iterable == null) {
            throw new NullPointerException("Iterator must not be null");
        }
        if (estimatedSize < 1) {
            throw new IllegalArgumentException("Estimated size must be greater than 0");
        }
    }

    private static final int DEFAULT_ESTIMATED_SIZE = 10;
}
